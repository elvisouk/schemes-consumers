package com.mint.schemescons;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchemesconsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SchemesconsApplication.class, args);
    }

}
