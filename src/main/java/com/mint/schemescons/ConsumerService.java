package com.mint.schemescons;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ConsumerService {

    public static final String KTOPIC = "com.ng.vela.even.card_verified";

    @KafkaListener(topics=KTOPIC, groupId = "card-schemes")
    public void pushDeliverEvent(String json) {

        log.info("*** Received message from {} content = '{}'", KTOPIC, json);

        // Do some other realtime stuff with payload
        // ...
    }

}
